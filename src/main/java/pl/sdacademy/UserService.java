package pl.sdacademy;

import java.util.HashMap;
import java.util.Map;

public class UserService {

    private static Map<String, String> users;

    public UserService() {
        users = new HashMap<>();
        users.put("jan", "pass");
        users.put("marek", "password");
    }

    public boolean isUserExists(String login, String password) {
        return users.containsKey(login) && users.get(login).equals(password);
    }
}
