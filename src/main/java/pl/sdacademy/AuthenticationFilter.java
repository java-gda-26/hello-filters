package pl.sdacademy;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebFilter(filterName = "authFilter")
public class AuthenticationFilter implements Filter {

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest httpRequest = (HttpServletRequest) request;
        HttpServletResponse httpResponse = (HttpServletResponse) response;
        HttpSession session = httpRequest.getSession();

        if (request.getParameter("logout") != null) {
            session.removeAttribute("currentUserLogin");
        }

        if (session.getAttribute("currentUserLogin") != null){
            chain.doFilter(request, response);
            return;
        }

        httpResponse.sendRedirect("/login");
    }
}
