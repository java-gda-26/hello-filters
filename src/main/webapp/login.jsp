<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <title>Login Page</title>
</head>
<body>
<h1>Login</h1>
<h2>${sessionScope.badCredentials ? "Podałeś błędne dane" : ""}</h2>
<form action="login" method="post">
    <label for="login">Login</label>
    <input type="text" id="login" name="login">
    <label for="password">Password</label>
    <input type="text" id="password" name="password">
    <input type="submit" value="Zaloguj">
</form>
</body>
</html>